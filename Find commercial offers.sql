SELECT
	[commercialOffer].[CommercialOfferId],
	[commercialOffer].[CommercialOfferTypeId],
	[commercialOffer].[Formula],
	[commercialOfferType].[Description],
	[commercialOfferTaxonomy].[TenantId],
	[commercialOfferType].[IsPolicyNumberRequired],
	[promotionCode].[Code],
	[commercialOffer].[CommercialOfferRatingCategoryId],
	[commercialOffer].[OfferName],
	[commercialOffer].[TitleText],
	[commercialOffer].[HelpText],
	[commercialOffer].[Description],
	[commercialOffer].[PartnerCode],
	[commercialOffer].[Formula],
	[commercialOffer].[StartDate],
	[commercialOffer].[EndDate],
	[commercialOffer].[CalculationTypeId],
	[commercialOffer].[CalculationValue],
	[commercialOffer].[LongevityId],
	[commercialOffer].[LegalMentions],
	[commercialOffer].[ExtendedEndDate]
FROM
	[AGDF_CommercialOffers].[dbo].[CommercialOffer] [commercialOffer]
	LEFT JOIN [AGDF_CommercialOffers].[dbo].[CommercialOfferTaxonomy] [commercialOfferTaxonomy]
		ON [commercialOffer].[CommercialOfferId] = [commercialOfferTaxonomy].[CommercialOfferId]
	LEFT JOIN [AGDF_CommercialOffers].[dbo].[CommercialOfferType] [commercialOfferType]
		ON [commercialOffer].[CommercialOfferTypeId] = [commercialOfferType].[CommercialOfferTypeId]
	LEFT JOIN [AGDF_CommercialOffers].[dbo].[PromotionCode] [promotionCode]
		ON [commercialOffer].[CommercialOfferId] = [promotionCode].[CommercialOfferId]
WHERE
	1 = 1
	AND GETDATE() BETWEEN [commercialOffer].[StartDate] AND ISNULL([commercialOffer].[EndDate], DATEADD(DAY, 1, GETDATE()))
	--AND [promotionCode].[Code] = 'DIRECTPARRAIN'
	AND [commercialOfferTaxonomy].[TenantId] = 54
ORDER BY
	[commercialOffer].[EndDate] DESC
