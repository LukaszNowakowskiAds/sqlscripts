---- Remove
--USE [gbi_resource]
--GO
--DROP EVENT SESSION [EXEC_CoveragesRetrieval] ON SERVER

---- Create
--USE [gbi_resource]
--GO
--CREATE EVENT SESSION [EXEC_CoveragesRetrieval] ON SERVER
--ADD EVENT sqlserver.sp_statement_completed(SET collect_object_name=(1), collect_statement=(1)
--ACTION (sqlserver.client_app_name, sqlserver.client_hostname, sqlserver.database_id, sqlserver.database_name, sqlserver.username)
--WHERE (([object_Type]=(8272)) AND ([source_database_id]=(16))))
--ADD TARGET package0.asynchronous_file_target
--	(
--		SET FILENAME = N'C:\Audits\EXEC_CoveragesRetrieval.xel',
--		METADATAFILE = N'C:\Audits\EXEC_CoveragesRetrieval.xem'
--	);
--GO

---- Start
--ALTER EVENT SESSION [EXEC_CoveragesRetrieval] ON SERVER
--	STATE = START;
--GO

---- Stop
--ALTER EVENT SESSION [EXEC_CoveragesRetrieval] ON SERVER
--	STATE = STOP;
--GO
