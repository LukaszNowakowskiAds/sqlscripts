USE [WaterDrinkingStatistics.Operations]
GO

;WITH [CTE_Data]
AS
(
	SELECT
		[Id],
		CONVERT(DATE, [ConsumptionDate]) [ConsumptionDate],
		[ConsumptionDate] [Timestamp]
		FROM
			[Consumption].[Drinks]
)
SELECT TOP 7
	[ConsumptionDate],
	COUNT([Id]) [Glasses]
	FROM
		[CTE_Data]
	GROUP BY
		[ConsumptionDate]
	ORDER BY
		[ConsumptionDate] DESC