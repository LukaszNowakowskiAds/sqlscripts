﻿USE [gbi_resource]
GO

WITH [PolicyVersions_CTE] AS
(
	SELECT
			ROW_NUMBER() OVER (PARTITION BY [policies].[policy_general_id] ORDER BY [versions].[policy_version_number] DESC) [RowNumber],
			[policies].[policy_general_id] [PolicyId],
			[versions].[policy_version_id] [VersionId],
			[policies].[policy_number] [PolicyNumber],
			[policies].[storage_date] [IssueDate]
		FROM
			[dbo].[policy_general] [policies]
			JOIN [dbo].[policy_version] [versions]
				ON [policies].[policy_general_id] = [versions].[policy_general_id]
		WHERE
			[policies].[TaxonomyId] = 55
)
SELECT
	[policyVersions].[PolicyId] [PolicyGeneralId],
	[telematicsContracts].[IMEI] [IMEI],
	[policyVersions].[PolicyNumber] [PolicyNumber],
	[telematicsContracts].[DriveboxSendDate] [DriveboxSendDate],
	[policyVersions].[IssueDate] [IssueDate],
	[workItems].[Status],
	CASE [WorkItems].[Status]
		WHEN 1 THEN 'En attente envoi de DriveBox'
		WHEN 2 THEN 'DriveBox à envoyer'
		WHEN 3 THEN 'DriveBox envoyée'
		WHEN 5 THEN 'DriveBox installée'
		WHEN 6 THEN '1ère relance installation DriveBox'
		WHEN 7 THEN '2ème relance installation DriveBox - Tâche planifiée'
		WHEN 8 THEN '2ème relance installation DriveBox'
		WHEN 9 THEN '3ème relance installation DriveBox - Tâche planifiée'
		WHEN 10 THEN '3ème relance installation DriveBox'
		WHEN 11 THEN 'Désactivation – Contrat Auto Connectée planifiée'
		WHEN 12 THEN 'Contrat Auto Standard'
		WHEN 13 THEN 'DriveBox installée'
		WHEN 14 THEN 'DriveBox débranchée'
		WHEN 15 THEN 'Remise possible réactivé'
		WHEN 17 THEN 'DriveBox en attente de retour'
		WHEN 23 THEN 'DriveBox renvoyée'
		WHEN 27 THEN 'DriveBox non récupérée'
	END AS Flag2,
	[contracts].[OriginalQuoteCreatedBy]
	FROM
		[PolicyVersions_CTE] [policyVersions]
		LEFT JOIN [telematics].[Contract] [telematicsContracts]
			ON [policyVersions].[PolicyId] = [telematicsContracts].[PolicyGeneralId]
		JOIN [WorkItems].[tasks].[WorkItems] [workItems]
			ON [policyVersions].[PolicyId] = [workItems].[PolicyGeneralId]
				AND [workItems].[WorkItemTypeId] = 18
		JOIN [agdf].[ObjectContract] [contracts]
			ON [policyVersions].[VersionId] = [contracts].[PolicyVersionId]
	WHERE
		[policyVersions].[RowNumber] = 1
	ORDER BY
		[policyVersions].[IssueDate] ASC