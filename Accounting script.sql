USE [gbi_resource]
GO

DECLARE @PolicyNumber int = 801009316

DECLARE @PolicyGeneralId int

SELECT @PolicyGeneralId = pg.policy_general_id FROM dbo.policy_general pg WHERE pg.policy_number = @PolicyNumber

--SET @PolicyGeneralId = 82975

--SELECT 'policy_general' as [Table], * FROM dbo.policy_general pg WHERE pg.policy_general_id = @PolicyGeneralId

--SELECT 'Invoice' as [Table], * FROM accounting.Invoice i WHERE i.PolicyGeneralId = @PolicyGeneralId

--SELECT 'payments' as [Table], * FROM accounting.Payment i WHERE i.PolicyGeneralId = @PolicyGeneralId

--SELECT 'policyversion' as [Table], * FROM dbo.policy_version v WHERE v.policy_general_id = @PolicyGeneralId

--SELECT 'InterfaceStorage' as [Table], * FROM [gbi_transaction].[dbo].vProductInterfaceStorage pis WHERE pis.PolicyGeneralId = @PolicyGeneralId ORDER BY pis.PolicyGeneralId desc

SELECT 'InvoiceInstalment' as [Table], [InstalmentStatusId], * FROM accounting.InvoiceInstalment iv WHERE iv.InvoiceId in (SELECT InvoiceId FROM accounting.Invoice i WHERE i.PolicyGeneralId = @PolicyGeneralId)

--SELECT * FROM gbi_transaction.dbo.vProductInterfaceStorage vpis WHERE vpis.PolicyGeneralId =@PolicyGeneralId

--select 'AccountingEntry' as [Table], * from accounting.AccountingEntry ae where ae.InvoiceId = 41860 and InvoiceInstalmentId is null
--SELECT 
--	o.BankInternationalAccountReferenceNumber as [OriginalIban],
--	pv.*
--FROM
--	[agdf].[ObjectPolicyHolder] o
--	JOIN
--	[dbo].[policy_version] pv
--		ON o.PolicyVersionId = pv.policy_version_id
--	JOIN 
--	[dbo].[policy_general] pg
--		ON pv.policy_general_id = pg.policy_general_id
--where pg.policy_general_id = @PolicyGeneralId