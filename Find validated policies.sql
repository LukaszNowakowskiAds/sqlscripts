USE [GBI_resource]
GO

WITH cte_Data AS
	(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY [versions].[policy_general_id] ORDER BY [versions].[policy_version_number] DESC) [RowNumber],
			[policies].[policy_number],
			[versions].[policy_version_number],
			[versions].[policy_version_status_id],
			[statuses].[ShortDesc],
			[policies].[TaxonomyId]
			FROM
				[dbo].[policy_general] [policies]
				JOIN [dbo].[policy_version] [versions]
					ON [policies].[policy_general_id] = [versions].[policy_general_id]
				JOIN [dbo].[LUPolicyVersionStatus] [statuses]
					ON [versions].[policy_version_status_id] = [statuses].[PolicyVersionStatusId]
	)
	SELECT
		*
		FROM
			[cte_Data]
		WHERE
			[RowNumber] = 1
			AND [policy_version_status_id] = 96
			AND [TaxonomyId] = 55