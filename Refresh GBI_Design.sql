USE [master]
GO

DECLARE @DatabaseName NVARCHAR(50) = 'GBI_Design'
DECLARE @DataDirectory NVARCHAR(300)
DECLARE @LogsDirectory NVARCHAR(300)

SELECT
    @DataDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultDataPath')),
    @LogsDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultLogPath'))

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'GBI_Design')
BEGIN
	ALTER DATABASE [GBI_Design] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [GBI_Design]
END

DECLARE @DataFile NVARCHAR(300) = @DataDirectory + @DatabaseName + '.mdf'
DECLARE @LogsFile NVARCHAR(300) = @LogsDirectory + @DatabaseName + '_log.ldf'

DECLARE @BackupPath NVARCHAR(200) = 'C:\Backups\GBI_Design.bak'
RESTORE DATABASE [GBI_Design]
	FROM DISK = @BackupPath
	WITH
		MOVE N'GBI_Design' TO @DataFile,
		MOVE N'GBI_Design_log' TO @LogsFile;
GO

USE [GBI_Design]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO
