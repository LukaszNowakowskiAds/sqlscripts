USE [gbi_resource]
GO

SELECT
	[policies].[policy_number] [Policy number],
	CASE [policies].[TaxonomyId]
		WHEN 52 THEN 'DA Standard'
		WHEN 55 THEN 'YouDrive'
		ELSE CONVERT(NVARCHAR, [policies].[TaxonomyId])
	END [Product],
	[policies].[storage_date] [Issue date]
	FROM
		[dbo].[policy_general] [policies] WITH (NOLOCK)
	WHERE
		[policies].[PortalId] = 2
	ORDER BY
		[policies].[policy_number] ASC