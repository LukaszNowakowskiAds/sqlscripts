--ALTER TABLE [JobQueue].[dbo].[JobQueueItemMetadata]
--	DROP CONSTRAINT [FK_JobQueueItems_JobQueueItemId]
--GO

--TRUNCATE TABLE [JobQueue].[dbo].[JobQueueItemMetadata]
--TRUNCATE TABLE [gbi_resource].[documents].[DocumentQueue]
--TRUNCATE TABLE [JobQueue].[dbo].[JobQueueItems]
--GO

--ALTER TABLE [JobQueue].[dbo].[JobQueueItemMetadata]
--	ADD CONSTRAINT [FK_JobQueueItems_JobQueueItemId]
--	FOREIGN KEY([JobQueueItemId])
--	REFERENCES [dbo].[JobQueueItems]([JobQueueItemId])
--GO

--TRUNCATE TABLE [WorkItems].[tasks].[WorkItemWakeupCalls]

  SELECT * FROM [gbi_resource].[documents].[DocumentQueue]
  SELECT * FROM [JobQueue].[dbo].[JobQueueItems]
  SELECT * FROM [WorkItems].[tasks].[WorkItemWakeupCalls]