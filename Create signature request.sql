USE [GBI_resource]
GO

DECLARE @VersionId INT = 119595

DECLARE @DocumentId INT
SELECT @DocumentId = [PolicyRequestedDocumentId] FROM [agdf].[PolicyRequestedDocument] WHERE [PolicyVersionId] = @VersionId AND [RequestedDocumentTypeId] = 7

DECLARE @TransactionId NVARCHAR(60)
SET @TransactionId = 'Signature_' + CONVERT(NVARCHAR, @VersionId) + '_' + CONVERT(NVARCHAR, @DocumentId)

EXEC [agdf].[ElectronicSignatureResponseInsert]
	@PolicyVersionId = @VersionId,
	@PolicyRequestedDocumentId = @DocumentId,
	@ElectronicSignatureProviderId = 2,
	@ElectronicSignatureTransactionId = @TransactionId,
	@ElectronicSignatureStatusId = 0

SELECT TOP 10 * FROM [agdf].[ElectronicSignatureResponse] ORDER BY [StorageDate] DESC

