USE [gbi_resource]
GO

UPDATE [dbo].[person]
	SET
		[password] = '$2a$10$rdXYI1sOqZFFDFIto0q0d.JOfMIRPvmCZaHZ21DdwV.kfb2h4erJy',
		[HasOnlineAccess] = 1

UPDATE [agdf].[UserCredentials]
	SET
		[password] = '$2a$10$rdXYI1sOqZFFDFIto0q0d.JOfMIRPvmCZaHZ21DdwV.kfb2h4erJy'

UPDATE [agdf].[User]
	SET
		[HasOnlineAccess] = 1

