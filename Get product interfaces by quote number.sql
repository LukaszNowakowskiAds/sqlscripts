USE [gbi_transaction]
GO

DECLARE @QuoteNumber NVARCHAR(20) = '1000594935'

DECLARE @ProductInterfaceStorageId INT

SELECT
	@ProductInterfaceStorageId = [ProductInterfaceStorageId]
	FROM
		[dbo].[ProductInterfaceStorageAttribute]
	WHERE
		[ProductAttributeId] = 11045
		AND [ProductAttributeValue] = @QuoteNumber

SELECT
	*
	FROM
		[dbo].[vProductInterfaceStorage]
	WHERE
		[ProductInterfaceStorageId] = @ProductInterfaceStorageId

SELECT
	*
	FROM
		[dbo].[vProductInterfaceStorageVersion]
	WHERE
		[ProductInterfaceStorageId] = @ProductInterfaceStorageId
	ORDER BY
		[StorageDate] DESC