/****************************************************/
/* Created by: SQL Server 2019 Profiler          */
/* Date: 05/19/2021  11:23:08 AM         */
/****************************************************/

DECLARE @CommandTextFilter NVARCHAR(200) = N'%FetchWorkItemsBySearchCriteria%'

-- Create a Queue
declare @rc int
declare @TraceID int
declare @maxfilesize bigint
set @maxfilesize = 5 

-- Please replace the text InsertFileNameHere, with an appropriate
-- filename prefixed by a path, e.g., c:\MyFolder\MyTrace. The .trc extension
-- will be appended to the filename automatically. If you are writing from
-- remote server to local drive, please use UNC path and make sure server has
-- write access to your network share

DECLARE @Timestamp NVARCHAR(MAX) = FORMAT(GETDATE(), 'yyyy.MM.dd-hh.mm.ss')
DECLARE @FileName NVARCHAR(256) = N'C:\Traces\SqlTrace-' + @Timestamp

exec @rc = sp_trace_create @TraceID output, 0, @FileName, @maxfilesize, NULL 
if (@rc != 0) goto error

-- Client side File and Table cannot be scripted

-- Set the events
declare @on bit
set @on = 1
exec sp_trace_setevent @TraceID, 10, 1, @on
exec sp_trace_setevent @TraceID, 10, 9, @on
exec sp_trace_setevent @TraceID, 10, 2, @on
exec sp_trace_setevent @TraceID, 10, 10, @on
exec sp_trace_setevent @TraceID, 10, 6, @on
exec sp_trace_setevent @TraceID, 10, 11, @on
exec sp_trace_setevent @TraceID, 10, 12, @on
exec sp_trace_setevent @TraceID, 10, 13, @on
exec sp_trace_setevent @TraceID, 10, 14, @on
exec sp_trace_setevent @TraceID, 10, 15, @on
exec sp_trace_setevent @TraceID, 10, 16, @on
exec sp_trace_setevent @TraceID, 10, 17, @on
exec sp_trace_setevent @TraceID, 10, 18, @on


-- Set the Filters
declare @intfilter int
declare @bigintfilter bigint

exec sp_trace_setfilter @TraceID, 1, 0, 6, @CommandTextFilter
exec sp_trace_setfilter @TraceID, 10, 0, 7, N'SQL Server Profiler - 59f7023d-0a20-423a-ad60-f4bdaa04cf47'
-- Set the trace status to start
exec sp_trace_setstatus @TraceID, 1

-- display trace id for future references
select TraceID=@TraceID
goto finish

error: 
select ErrorCode=@rc

finish: 
go

-- Stop trace
--exec sp_trace_setstatus 2, 0

-- Delete trace
--exec sp_trace_setstatus 2, 2