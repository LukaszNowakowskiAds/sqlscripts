USE [AGDF_Mario_Migration]
GO

TRUNCATE TABLE [Staging].[MappedPA]
TRUNCATE TABLE [ToDarwin].[PoliciesToBeMigrated]
TRUNCATE TABLE [Reporting].[MigrationLogs]
TRUNCATE TABLE [Reporting].[MappingLegacyToDarwinLogReport]
TRUNCATE TABLE [JobQueue].[dbo].[JobQueueItems]
TRUNCATE TABLE [gbi_resource].[documents].[DocumentQueue]

UPDATE
	[gbi_resource].[dbo].[policy_general]
	SET
		[ExternalPolicyNumber] = ''
	WHERE
		[TaxonomyId] = 54

UPDATE
	[AGDF_Mario_Migration].[ToDarwin].[PoliciesToBeMigrated]
	SET
		[Status] = 6
