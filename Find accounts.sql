USE [gbi_resource]
GO

SELECT TOP 20
	[person].[relation_id],
	[person].[EmailAddress],
	[person].[users_name],
	[person].[password],
	[person].[HasOnlineAccess],
	[policy].[policy_number],
	[policy].[initial_startdate],
	[policy].[TaxonomyId]
	FROM
		[dbo].[person] [person]
		JOIN [dbo].[policy_general] [policy]
			ON [person].[relation_id] = [policy].[relation_id]
	WHERE
		[policy].[TaxonomyId] = 55
		--[person].[relation_id] IN (54129, 10714)
		AND [person].[EmailAddress] = 'lukasz.nowakowski@axadirect-solutions.pl'
	ORDER BY
		[policy].[initial_startdate] DESC
