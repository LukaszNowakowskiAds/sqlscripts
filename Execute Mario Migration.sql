USE [AGDF_Mario_Migration]
GO

TRUNCATE TABLE [Staging].[MappedPA]
TRUNCATE TABLE [ToDarwin].[PoliciesToBeMigrated]
TRUNCATE TABLE [Reporting].[MigrationLogs]
TRUNCATE TABLE [Reporting].[MappingLegacyToDarwinLogReport]
TRUNCATE TABLE [JobQueue].[dbo].[JobQueueItems]

EXEC [Staging].[PerformDataCheck]
EXEC [Staging].[PerformMappingLegacyToDarwin]
EXEC [ToDarwin].[PopulatePoliciesToBeMigrated]