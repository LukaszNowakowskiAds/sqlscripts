USE [master]
GO

DECLARE @DatabaseName NVARCHAR(50) = 'AGDF_External'
DECLARE @DataDirectory NVARCHAR(300)
DECLARE @LogsDirectory NVARCHAR(300)

SELECT
    @DataDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultDataPath')),
    @LogsDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultLogPath'))

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'AGDF_External')
BEGIN
	ALTER DATABASE [AGDF_External] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [AGDF_External]
END

DECLARE @DataFile NVARCHAR(300) = @DataDirectory + @DatabaseName + '.mdf'
DECLARE @LogsFile NVARCHAR(300) = @LogsDirectory + @DatabaseName + '_log.ldf'

DECLARE @BackupPath NVARCHAR(200) = 'C:\Backups\AGDF_External.bak'
RESTORE DATABASE [AGDF_External]
	FROM DISK = @BackupPath
	WITH
		MOVE N'AGDF_External' TO @DataFile,
		MOVE N'AGDF_External_log' TO @LogsFile;
GO

USE [AGDF_External]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [AGDF_External]
GO
DBCC SHRINKFILE (N'AGDF_External_log' , 0, TRUNCATEONLY)
DBCC SHRINKFILE (N'AGDF_External' , 0, TRUNCATEONLY)
DBCC SHRINKDATABASE(N'AGDF_External' )
GO
