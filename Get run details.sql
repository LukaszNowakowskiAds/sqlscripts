USE [AGDF_Logging]
GO

DECLARE @RunId BIGINT = 16

SELECT
	[runs].[Id] [Run id],
	[processors].[DisplayName] [Processor name],
	[runs].[Description] [Run description],
	[runs].[StartDate] [Processing start],
	[runs].[EndDate] [Processing end],
	[statuses].[DisplayName] [Run status]
	FROM
		[Processing].[Runs] [runs]
		JOIN [Processing].[Processors] [processors]
			ON [runs].[ProcessorId] = [processors].[Id]		
		JOIN [Processing].[Statuses] [statuses]
			ON [runs].[StatusId] = [statuses].[Id]
	WHERE
		[runs].[Id] = @RunId

DECLARE itemsCursor CURSOR FOR
	SELECT
		[items].[Id] [ItemId],
		[items].[ExternalId] [Item identifier],
		[items].[StartDate] [Processing start],
		[items].[EndDate] [Processing end],
		[statuses].[DisplayName] [Processing status]
		FROM
			[Processing].[Items] [items]
			JOIN [Processing].[Statuses] [statuses]
				ON [items].[StatusId] = [statuses].[Id]
		WHERE
			[RunId] = @RunId

OPEN itemsCursor
DECLARE @ItemId BIGINT,
	@ExternalId NVARCHAR(100),
	@StartDate DATETIME2,
	@EndDate DATETIME2,
	@Status NVARCHAR(100),
	@ErrorId BIGINT

FETCH NEXT FROM itemsCursor INTO @ItemId, @ExternalId, @StartDate, @EndDate, @Status

WHILE (@@FETCH_STATUS = 0)
BEGIN
	SELECT
		@ExternalId [Item identifier],
		@StartDate [Processing start],
		@EndDate [Processing end],
		@Status [Processing status]

	SELECT
		[details].[Timestamp] [Event date],
		[details].[Message] [Message logged]
		FROM
			[Processing].[ItemDetails] [details]
		WHERE
			[details].[ItemId] = @ItemId
		ORDER BY
			[details].[Timestamp] ASC

	FETCH NEXT FROM itemsCursor INTO @ItemId, @ExternalId, @StartDate, @EndDate, @Status
END

CLOSE itemsCursor
DEALLOCATE itemsCursor