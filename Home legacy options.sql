USE [gbi_resource]
GO

SELECT
	[covers].[GardenSelectedLevel],
	[covers].[SwimmingpoolSelectedLevel]
	FROM
		[dbo].[policy_general] [policies]
		JOIN [dbo].[policy_version] [versions]
			ON [policies].[policy_general_id] = [versions].[policy_general_id]
		JOIN [agdf].[ObjectHomeCoverInfo] [covers]
			ON [versions].[policy_version_id] = [covers].[PolicyVersionId]
	WHERE
		[policies].[ExternalPolicyNumber] = '48667268'