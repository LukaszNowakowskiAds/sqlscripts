USE [gbi_resource]
GO

SELECT
	[statuses].[ShortDesc],
	COUNT(*) [Count]
	FROM
		[documents].[DocumentQueue] [items]
		JOIN [documents].[LUDocumentQueueStatus] [statuses]
			ON [items].[StatusId] = [statuses].[DocumentQueueStatusId]
	GROUP BY
		[statuses].[ShortDesc]
	ORDER BY
		COUNT(*) DESC
GO
