USE [gbi_resource]
GO

SELECT
	--[versions].[policy_version_number],
	--[versions].[storage_date],
	[transactions].[ProductInterfaceStorageId],
	[transactions].[StorageDate],
	[transactions].[ModificationDate]
	FROM
		[dbo].[policy_general] [policies]
		--JOIN [dbo].[policy_version] [versions]
		--	ON [policies].[policy_general_id] = [versions].[policy_general_id]
		JOIN [GBI_transaction].[dbo].[ProductInterfaceStorage] [transactions]
			ON [policies].[policy_general_id] = [transactions].[PolicyGeneralId]
	WHERE
		[policies].[policy_number] = '807259951'
	ORDER BY
		[transactions].[StorageDate] ASC