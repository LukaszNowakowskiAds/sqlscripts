USE [JobQueue]
GO

--UPDATE [dbo].[JobQueueItems]
--	SET
--		[StatusId] = 0
--	WHERE
--		[JobQueueItemId] = 7

SELECT
	[items].[JobQueueItemId],
	[statuses].[JobQueueItemStatusDescription],
	[items].[Command],
	[items].[StorageDate]
	FROM
		[dbo].[JobQueueItems] [items]
		JOIN [dbo].[LUJobQueueItemStatus] [statuses]
			ON [items].[StatusId] = [statuses].[JobQueueItemStatusId]
	ORDER BY
		[items].[StorageDate] ASC
