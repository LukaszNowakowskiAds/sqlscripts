USE [GBI_Logging]
GO

DECLARE @DivisionDate DATETIME2 = '2023-08-30 13:40:00';

WITH [CTE_RawData] AS
(
	SELECT
		[audit].[Id],
		[audit].[Url],
		[audit].[RequestTime],
		[audit].[ResponseTime],
		[audit].[ProcessingTime],
		[audit].[ResponseCodeId],
		CASE
			WHEN [audit].[RequestTime] < @DivisionDate THEN CONVERT(BIT, 1)
			ELSE CONVERT(BIT, 0)
		END [IsFirstBatch]
	FROM
		[dbo].[HttpMessageAudit] [audit]
	WHERE
		[Url] LIKE '%/guarantees'
		AND [RequestTime] BETWEEN '2023-08-30 13:30:00' AND '2023-08-31 23:59:59'
),
[CTE_Positions] AS
(
	SELECT
		[data].[Id],
		[data].[Url],
		(CHARINDEX('/policies/', [data].[Url]) + LEN('/policies/')) [PolicyIdStartPosition],
		CHARINDEX('/guarantees', [data].[Url]) [PolicyIdEndPosition],
		[data].[RequestTime],
		[data].[ResponseTime],
		[data].[ProcessingTime],
		[data].[ResponseCodeId],
		[data].[IsFirstBatch]
	FROM
		[CTE_RawData] [data]
),
[CTE_EncryptedPolicyId] AS
(
	SELECT
		[data].[Id],
		[data].[Url],
		SUBSTRING([data].[Url], [data].[PolicyIdStartPosition], [data].[PolicyIdEndPosition] - [data].[PolicyIdStartPosition]) [EncryptedPolicyGeneralId],
		[data].[RequestTime],
		[data].[ResponseTime],
		[data].[ProcessingTime],
		[data].[ResponseCodeId],
		[data].[IsFirstBatch]
	FROM
		[CTE_Positions] [data]
)
SELECT
	*
FROM
	[CTE_EncryptedPolicyId] [data]
ORDER BY
	[data].[RequestTime] DESC