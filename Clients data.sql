USE [gbi_resource]
GO

DECLARE @PolicyNumber NVARCHAR(20) = '800000316'

SELECT
	[versions].[policy_version_id],
	[drivers].[RoleId],
	[drivers].[UniqueId],
	[drivers].[FirstName],
	[drivers].[LastName],
	[drivers].[BirthDate],
	[drivers].[IsCarOwner],
	[cars].[OwnerFirstName],
	[cars].[OwnerLastName],
	[cars].[OwnerBirthDate],
	[cars].[OwnerUniqueId]
	FROM
		[dbo].[policy_general] [policies]
		JOIN [dbo].[policy_version] [versions]
			ON [policies].[policy_general_id] = [versions].[policy_general_id]
		JOIN [agdf].[ObjectDriver] [drivers]
			ON [versions].[policy_version_id] = [drivers].[PolicyVersionId]
		JOIN [agdf].[ObjectCar] [cars]
			ON [versions].[policy_version_id] = [cars].[PolicyVersionId]
	WHERE
		[policies].[policy_number] = @PolicyNumber
		OR [policies].[ExternalPolicyNumber] = @PolicyNumber
	ORDER BY
		[drivers].[LastName] ASC,
		[drivers].[FirstName] ASC,
		[drivers].[BirthDate] ASC,
		[versions].[policy_version_id] ASC