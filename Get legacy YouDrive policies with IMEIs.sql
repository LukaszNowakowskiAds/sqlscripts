USE [gbi_resource]
GO

WITH [CTE_FoundPolicies] AS
(
    SELECT
        ROW_NUMBER() OVER (PARTITION BY [policies].[policy_general_id] ORDER BY [versions].[policy_version_number] DESC) [RowNumber],
        [policies].[policy_number] [Policy number],
        CASE [policies].[TaxonomyId]
            WHEN 52 THEN 'DA Standard'
            WHEN 55 THEN 'YouDrive'
            ELSE CONVERT(NVARCHAR, [policies].[TaxonomyId])
        END [Product],
        [policies].[storage_date] [Issue date],
        [versions].[start_date] [Start date],
        [versions].[end_date] [End date],
        ISNULL([telematicsContracts].[IMEI], '---') [IMEI number]
    FROM
        [dbo].[policy_general] [policies] WITH (NOLOCK)
        JOIN [dbo].[policy_version] [versions] WITH (NOLOCK)
            ON [policies].[policy_general_id] = [versions].[policy_general_id]
        LEFT JOIN [telematics].[Contract] [telematicsContracts] WITH (NOLOCK)
            ON [policies].[policy_general_id] = [telematicsContracts].[PolicyGeneralId]
    WHERE
        [policies].[PortalId] = 2
)
SELECT
    [policies].[Policy number],
    [policies].[Product],
    [policies].[Issue date],
    [policies].[Start date],
    [policies].[End date],
    [policies].[IMEI number]
    FROM
        [CTE_FoundPolicies] [policies]
    WHERE
        [policies].[RowNumber] = 1
    ORDER BY
        [policies].[Policy number] ASC