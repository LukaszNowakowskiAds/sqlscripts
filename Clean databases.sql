USE [master]
GO

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'AGDF_Documents')
BEGIN
	ALTER DATABASE [AGDF_Documents] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [AGDF_Documents]
END

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'AGDF_InsuranceCompanies')
BEGIN
	ALTER DATABASE [AGDF_InsuranceCompanies] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [AGDF_InsuranceCompanies]
END

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'AGDF_SecurityAudit')
BEGIN
	ALTER DATABASE [AGDF_SecurityAudit] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [AGDF_SecurityAudit]
END

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'Export')
BEGIN
	ALTER DATABASE [Export] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [Export]
END

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'GBI_Logging')
BEGIN
	ALTER DATABASE [GBI_Logging] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [GBI_Logging]
END
GO

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'gbi_resource')
BEGIN
	ALTER DATABASE [gbi_resource] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [gbi_resource]
END
GO

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'gbi_transaction')
BEGIN
	ALTER DATABASE [gbi_transaction] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [gbi_transaction]
END
GO

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'JobQueue')
BEGIN
	ALTER DATABASE [JobQueue] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [JobQueue]
END
GO

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'WorkItems')
BEGIN
	ALTER DATABASE [WorkItems] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [WorkItems]
END
GO

CREATE DATABASE [AGDF_Documents]
CREATE DATABASE [AGDF_InsuranceCompanies]
CREATE DATABASE [AGDF_SecurityAudit]
CREATE DATABASE [Export]
CREATE DATABASE [GBI_Logging]
CREATE DATABASE [gbi_resource]
CREATE DATABASE [gbi_transaction]
CREATE DATABASE [JobQueue]
CREATE DATABASE [WorkItems]
GO

USE [AGDF_Documents]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [AGDF_InsuranceCompanies]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [AGDF_SecurityAudit]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [Export]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [GBI_Logging]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [gbi_resource]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [gbi_transaction]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [JobQueue]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [WorkItems]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

--DECLARE @EmailAddress NVARCHAR(100) = 'lukasz.nowakowski@axadirect-solutions.pl'
--DECLARE @FirstName NVARCHAR(50) = 'Lukasz'
--DECLARE @LastName NVARCHAR(50) = 'NOWAKOWSKI'
--DECLARE @WindowsUser NVARCHAR(50) = 'ADS-PL\l.nowakowski'

--IF EXISTS (SELECT * FROM [WorkItems].[operations].[CustomerServiceUser] WHERE [UserName] = @EmailAddress)
--BEGIN
--	UPDATE [WorkItems].[operations].[CustomerServiceUser]
--		SET
--			[UserName] = @WindowsUser,
--			[IsTester] = 0,
--			[IsActive] = 1,
--			[FirstName] = @FirstName,
--			[LastName] = @LastName,
--			[HasThermometerPermission] = 0,
--			[CommercialOfferPermissionFlag] = 3,
--			[HasRetentionOfferPermission] = 1,
--			[HasSplitPolicyPermission] = 1,
--			[HasPaymentBySmsPermission] = 1,
--			[HasVersionCancellationPermission] = 1,
--			[HasAgentSponsorDashboardFullPermission] = 1,
--			[HasAgentVipStatusPermission] = 1,
--			[HasGdprQuoteDeletionPermission] = 1,
--			[IdWindows] = @WindowsUser,
--			[HasExclusiveCancellationPermission] = 1,
--			[EmailAddress] = @EmailAddress,
--			[IsNewCustomerServiceUser] = 1

--		WHERE
--			[UserName] = @EmailAddress
--END

--IF NOT EXISTS (SELECT * FROM [WorkItems].[operations].[CustomerServiceUser] WHERE [UserName] = @WindowsUser)
--BEGIN
--INSERT INTO [WorkItems].[operations].[CustomerServiceUser]
--	([UserName], [IsTester], [IsActive], [FirstName], [LastName], [HasThermometerPermission], [CommercialOfferPermissionFlag],
--	[HasRetentionOfferPermission], [HasSplitPolicyPermission], [HasPaymentBySmsPermission], [HasVersionCancellationPermission],
--	[HasAgentSponsorDashboardFullPermission], [HasAgentVipStatusPermission], [HasGdprQuoteDeletionPermission], [IdWindows],
--	[HasExclusiveCancellationPermission], [EmailAddress], [IsNewCustomerServiceUser])
--	VALUES
--	(@WindowsUser, 0, 1, @FirstName, @LastName, 0, 3, 1, 1, 1, 1, 1, 1, 1, @WindowsUser, 1, @EmailAddress, 1)
--END