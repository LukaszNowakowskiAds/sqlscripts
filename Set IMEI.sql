USE [gbi_resource]
GO

DECLARE @RelationId INT
SELECT
	@RelationId = [relation_id]
	FROM
		[dbo].[person]
	WHERE
		[EmailAddress] = 'lukasz.nowakowski@axadirect-solutions.pl'

DECLARE @PolicyGeneralId INT
SELECT
	@PolicyGeneralId = [policy_general_id]
	FROM
		[dbo].[policy_general]
	WHERE
		[relation_id] = @RelationId
		AND [TaxonomyId] = 55

UPDATE [telematics].[Contract]
	SET
		[IMEI] = '351631045664882F',
		[DriveboxDemandDate] = '2016-09-21',
		[DriveboxSendDate] = '2016-09-22',
		[DriveboxReceiveDate] = '2016-09-23',
		[DriveboxInstallationDate] = '2016-09-24'
	WHERE
		[PolicyGeneralId] = @PolicyGeneralId

SELECT
	*
	FROM
		[telematics].[Contract]
	WHERE
		[PolicyGeneralId] = @PolicyGeneralId