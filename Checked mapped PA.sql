USE [AGDF_Mario_Migration]
GO

--TRUNCATE TABLE [Reporting].[MappingLegacyToDarwinLogReport]

--EXEC [Staging].[PerformMappingLegacyToDarwin]

SELECT DISTINCT
	[legacy].[CNT_ID],
	[legacy].[INH_PREV_CANCELLATION_MOTIVE],
	[pas].[LegacyPolicyNumber],
	[pas].[Value],
	[legacyPolicy].[CNT_ID]
	FROM
		[FromLegacy].[LegacyPolicyPart4] [legacy]
		LEFT JOIN [Staging].[MappedPA] [pas]
			ON [legacy].[CNT_ID] = [pas].[LegacyPolicyNumber]
				AND [pas].[PAId] = 13058
		LEFT JOIN [FromLegacy].[LegacyPolicyPart1] [legacyPolicy]
			ON [legacy].[CNT_ID] = [legacyPolicy].[CNT_ID]
