USE [JobQueue];
GO
--SELECT * FROM [dbo].[LUJobQueueItemStatus]

SELECT
	[items].[JobQueueItemId],
	[items].[Command],
	[items].[StatusId],
	[configuration].[JobRunnerId],
	[items].[RetryCount]
	FROM
		[dbo].[JobQueueItems] [items]
		LEFT JOIN [dbo].[JobRunnerConfiguration] [configuration]
			ON [items].[JobTypeId] = [configuration].[JobTypeId]
	--WHERE
	--	[StatusId] = 0
	ORDER BY
		[items].[StatusId] ASC,
		[configuration].[JobRunnerId] ASC