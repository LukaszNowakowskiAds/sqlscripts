USE [gbi_resource]
GO

WITH [CTE_Flats] AS
(
	SELECT
		[PolicyVersionId]
		FROM
			[agdf].[ObjectHome]
		WHERE
			[AccomodationHomeTypeId] = 1
			AND [OccupantTypeId] = 1
),
[CTE_MigratedPolicies] AS
(
	SELECT
		[PolicyVersionId]
		FROM
			[agdf].[ObjectContract]
		WHERE
			[IsMigratedPolicy] = 1
),
[CTE_HomePolicies] AS
(
	SELECT
		[versions].[policy_version_id] [PolicyVersionId]
		FROM
			[dbo].[policy_general] [policies]
			JOIN [dbo].[policy_version] [versions]
				ON [policies].[policy_general_id] = [versions].[policy_general_id]
		WHERE
			[policies].[TaxonomyId] = 54
)
SELECT
	[policies].[policy_number] [Policy number]
	FROM
		[dbo].[policy_general] [policies]
		JOIN [dbo].[policy_version] [versions]
			ON [policies].[policy_general_id] = [versions].[policy_general_id]
		JOIN [CTE_Flats] [flats]
			ON [versions].[policy_version_id] = [flats].[PolicyVersionId]
		JOIN [CTE_MigratedPolicies] [migrated]
			ON [versions].[policy_version_id] = [migrated].[PolicyVersionId]
		JOIN [CTE_HomePolicies] [homes]
			ON [versions].[policy_version_id] = [homes].[PolicyVersionId]
