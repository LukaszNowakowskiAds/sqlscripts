USE [gbi_resource]
GO

DECLARE @DocumentId NVARCHAR(50)
SET @DocumentId = 'XGSA01-PM008-20191107-122644'

DELETE FROM [interfaces].[XeroxDocumentPage] WHERE [XeroxDocumentId] IN (SELECT [XeroxDocumentId] FROM [interfaces].[XeroxDocument] WHERE [XeroxArchiveId] = @DocumentId)
DELETE FROM [interfaces].[XeroxDocument] WHERE [XeroxArchiveId] = @DocumentId
DELETE FROM [interfaces].[XeroxArchive] WHERE [XeroxArchiveId] = @DocumentId

SELECT * FROM [interfaces].[XeroxArchive]
SELECT * FROM [interfaces].[XeroxDocument]
SELECT * FROM [interfaces].[XeroxDocumentPage]
