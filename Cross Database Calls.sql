DECLARE @Databases TABLE
(
	[Name] NVARCHAR(200)
)

INSERT INTO @Databases
	([Name])
	VALUES
	('AGDF_CommercialOffers'),
	('AGDF_DaVinci_Migration'),
	('AGDF_Documents'),
	('AGDF_External'),
	('AGDF_InsuranceCompanies'),
	('AGDF_Mario_Migration'),
	('AGDF_Migration'),
	('AGDF_SecurityAudit'),
	('ASPState'),
	('EventQueue'),
	('Export'),
	('GBI_Design'),
	('GBI_Logging'),
	('gbi_resource'),
	('gbi_transaction'),
	('JobQueue'),
	('WorkItems');

WITH [CTE_RawData] AS
(
	SELECT
		OBJECT_SCHEMA_NAME([dep].[referencing_id]) [Schema],
		OBJECT_NAME([dep].[referencing_id]) [Name],
		[dep].[referenced_database_name] [ReferencedDb],
		[dep].[referenced_schema_name] [ReferencedSchema],
		[dep].[referenced_entity_name] [ReferencedEntity],
		CASE
			WHEN [dbs].[Name] IS NULL THEN 1
			ELSE 0
		END [IsInternal]
	FROM
		[sys].[sql_expression_dependencies] [dep]
		LEFT JOIN @Databases [dbs]
			ON [dep].[referenced_database_name] = [dbs].[Name]
	WHERE
		[dep].[referenced_database_name] <> DB_NAME() /*Not NULL and not current DB*/
		AND OBJECTPROPERTY([dep].[referencing_id], 'IsProcedure') = 1
)
SELECT
	CASE [data].[IsInternal]
		WHEN 0 THEN 'YES'
		ELSE ''
	END [Fix needed],
	[data].[Schema],
	[data].[Name],
	[data].[ReferencedDb],
	[data].[ReferencedSchema],
	[data].[ReferencedEntity]
FROM
	[CTE_RawData] [data]
ORDER BY
	[data].[Schema],
	[data].[Name]
