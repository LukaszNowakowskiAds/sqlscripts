USE [GBI_resource]
GO

WITH [cte_Data] AS
	(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY [policies].[policy_general_id] ORDER BY [versions].[policy_version_number] DESC) [RowNumber],
			[policies].[policy_number] [PolicyNumber],
			[policies].[policy_general_id] [PolicyId],
			[versions].[policy_version_status_id] [StatusId],
			[versions].[policy_version_id] [VersionId],
			[policies].[storage_date] [IssueDate],
			[persons].[EmailAddress],
			[policies].[ExternalPolicyNumber],
			[policies].[TaxonomyId]
			FROM
				[dbo].[policy_general] [policies]
				JOIN [dbo].[policy_version] [versions]
					ON [policies].[policy_general_id] = [versions].[policy_general_id]
				JOIN [dbo].[person] [persons]
					ON [policies].[relation_id] = [persons].[relation_id]
			WHERE
				[TaxonomyId] IN
				(
				--51, -- BNPP
				52, -- DA Standard
				--54, -- Home
				--55, -- YouDrive
				--56, -- Bla Bla Car
				0)
	)
	SELECT
		[data].[PolicyNumber],
		[data].[PolicyId],
		[data].[VersionId],
		[data].[IssueDate],
		[data].[EmailAddress],
		[data].[ExternalPolicyNumber],
		[data].[TaxonomyId]
		FROM
			[cte_Data] [data]
		WHERE
			[data].[RowNumber] = 1
			--AND [data].[StatusId] = 2 -- Contrat emis
			AND [data].[StatusId] = 96 -- Contrat valide
			--AND [data].[StatusId] = 32 -- Endorsement - Suspension - Reviewed & rejected
			--AND [data].[ExternalPolicyNumber] <> ''
		ORDER BY
			[data].[IssueDate] DESC