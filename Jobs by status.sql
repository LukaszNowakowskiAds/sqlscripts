USE [JobQueue];
GO
WITH [CTE_RawData] AS
(
SELECT
	[statuses].[JobQueueItemStatusId] [Status id],
	[statuses].[JobQueueItemStatusDescription] AS [Job item status],
	ISNULL([configuration].[JobRunnerId], 0) [JobRunnerId],
	COUNT([items].[JobQueueItemId]) AS Count
	FROM 
		[dbo].[JobQueueItems] AS [items]
			JOIN [dbo].[LUJobQueueItemStatus] AS [statuses]
				ON [items].[StatusId] = [statuses].[JobQueueItemStatusId]
			LEFT JOIN [dbo].[JobRunnerConfiguration] AS [configuration]
				ON [items].[JobTypeId] = [configuration].[JobTypeId]
	WHERE
		[items].[DueDate] <= GETDATE()
	GROUP BY
		[statuses].[JobQueueItemStatusDescription],
		[statuses].[JobQueueItemStatusId],
		[configuration].[JobRunnerId]
)
SELECT
	[data].[Job item status],
	CASE [data].[JobRunnerId]
		WHEN 0 THEN 'SyncJobEngine'
		ELSE 'JobEngine' + CONVERT(NVARCHAR, [data].[JobRunnerId])
	END [Runner],
	[data].[Count] [Items count]
	FROM
		[CTE_RawData] [data]
	ORDER BY
		[data].[JobRunnerId] ASC,
		[data].[Status id] ASC
GO
USE [gbi_resource];
GO
SELECT
	[statuses].[DocumentQueueStatusId] [Document status id],
	[statuses].[ShortDesc] AS [Document item status],
	COUNT([queue].[DocumentQueueId]) AS Count
	FROM 
		 [documents].[DocumentQueue] AS [queue]
		 JOIN [documents].[LUDocumentQueueStatus] AS [statuses]
			 ON [queue].[StatusId] = [statuses].[DocumentQueueStatusId]
	GROUP BY
		[statuses].[ShortDesc],
		[statuses].[DocumentQueueStatusId]
	ORDER BY
		[statuses].[DocumentQueueStatusId] ASC
GO
USE [WorkItems];
GO
WITH [CTE_RawData] AS
(
	SELECT
		[WorkItemId],
		DATEPART(YEAR, [WakeUpTime]) [Year],
		DATEPART(MONTH, [WakeUpTime]) [Month],
		DATEPART(DAY, [WakeUpTime]) [Day],
		DATEPART(HOUR, [WakeUpTime]) [Hour],
		DATEPART(MINUTE, [WakeUpTime]) [Minute],
		DATEPART(SECOND, [WakeUpTime]) [Second]
	FROM
		[tasks].[WorkItemWakeupCalls]
),
[CTE_ByDate] AS
(
	SELECT
		DATEFROMPARTS([raw].[Year], [raw].[Month], [raw].[Day]) [Date],
		[raw].[WorkItemId]
	FROM
		[CTE_RawData] [raw]
)
SELECT
	[byDate].[Date] [Wake-up date],
	COUNT([byDate].[WorkItemId]) [Work items count]
FROM
	[CTE_ByDate] [byDate]
GROUP BY
	[byDate].[Date]
ORDER BY
	[byDate].[Date] ASC
GO
