USE [gbi_resource]
GO

DELETE FROM [dbo].[Tax] WHERE [CoverageId] IN (SELECT [CoverageId] FROM [dbo].[coverage] WHERE [policy_version_id] IN (1750, 1751))
DELETE FROM [dbo].[Limit] WHERE [CoverageId] IN (SELECT [CoverageId] FROM [dbo].[coverage] WHERE [policy_version_id] IN (1750, 1751))
DELETE FROM [dbo].[Deductible] WHERE [CoverageId] IN (SELECT [CoverageId] FROM [dbo].[coverage] WHERE [policy_version_id] IN (1750, 1751))
DELETE FROM [dbo].[coverage] WHERE [policy_version_id] IN (1750, 1751)
DELETE FROM [documents].[DocumentQueue] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectPruning] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectProcess] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectCar] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectHomeTariffPreview] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectDriver] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectThermometer] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectPolicyHolder] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectPortfolioProtection] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectContract] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [dbo].[InvolvedObject] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectRenewal] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [agdf].[ObjectCoverInfo] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [dbo].[ProcessInfo] WHERE [PolicyVersionId] IN (1750, 1751)
DELETE FROM [dbo].[policy_version] WHERE [policy_version_id] IN (1750, 1751)

SELECT
	*
	FROM
		[dbo].[policy_version]
	WHERE
		[policy_general_id] = 1622

SELECT
	*
	FROM
		[dbo].[LUPolicyVersionStatus]
	WHERE
		[PolicyVersionStatusId] IN (2, 62, 96)