USE [AGDF_Mario_Migration]
GO

WITH [CTE_RawData] AS
(
	SELECT
		[pas1].[LegacyPolicyNumber],
		[pas1].[PAId],
		[pas1].[Value]
		FROM
			[Staging].[MappedPA] [pas1]
		WHERE
			[pas1].[PAId] = 122
			--AND SUBSTRING([pas1].[Value], 1, 7) = '2018-06'
)
SELECT TOP 5
	[rawData].[PAId],
	[rawData].[Value],
	--[rawData1].[PAId],
	--[rawData1].[Value],
	[policies].[LegacyPolicyNumber],
	[policies].[Status]
	FROM
		[CTE_RawData] [rawData]
		--JOIN [CTE_RawData1] [rawData1]
		--	ON [rawData].[LegacyPolicyNumber] = [rawData1].[LegacyPolicyNumber]
		JOIN [ToDarwin].[PoliciesToBeMigrated] [policies]
			ON [rawData].[LegacyPolicyNumber] = [policies].[LegacyPolicyNumber]
	WHERE
		[policies].[Status] IN (6, 2)
	ORDER BY
		[rawData].[LegacyPolicyNumber] ASC

--UPDATE [ToDarwin].[PoliciesToBeMigrated]
--	SET
--		[Status] = 2
--	WHERE
--		[LegacyPolicyNumber] IN (34265369)

SELECT
	*,
	CASE [Status]
		WHEN 1 THEN 'Other'
		WHEN 2 THEN 'Pending'
		WHEN 3 THEN 'Migrated'
		WHEN 4 THEN 'RejectionByEtl'
		WHEN 5 THEN 'RejectionByInjection'
		WHEN 6 THEN 'Excluded'
		WHEN 7 THEN 'DryRunSuccess'
		WHEN 8 THEN 'DryRunFailure'
		ELSE '---'
	END [StatusCode]
	FROM
		[ToDarwin].[PoliciesToBeMigrated] [policies]
	WHERE
		[policies].[LegacyPolicyNumber] = '34265369'

SELECT
	*
	FROM
		[gbi_resource].[dbo].[policy_general]
	WHERE
		[ExternalPolicyNumber] = '34265369'
