USE [AGDF_Mario_Migration]
GO

INSERT INTO [ToDarwin].[PoliciesToBeMigrated]
	([LegacyPolicyNumber], [Status], [StorageDate], [MigrationDate], [DarwinPolicyGeneralId], [PAXml])
	VALUES
	('28097868', 2, GETDATE(), NULL, NULL, '<Xml>
  <pa id="14" v="2" lv="2" ov="2" />
  <pa id="15" v="4" lv="4" ov="4" />
  <pa id="18" v="202" lv="202" ov="202" />
  <pa id="107" v="0" lv="0" ov="0" />
  <pa id="122" v="2018-04-01" lv="2018-04-01" ov="2018-04-01" />
  <pa id="124" v="2018-04-01" lv="2018-04-01" ov="2018-04-01" />
  <pa id="636" v="28097868" lv="28097868" ov="28097868" />
  <pa id="1515" v="2011-04-01" lv="2011-04-01" ov="2011-04-01" />
  <pa id="2126" v="4" lv="4" ov="4" />
  <pa id="2184" v="2222222222" lv="2222222222" ov="2222222222" />
  <pa id="2854" v="21" lv="21" ov="21" />
  <pa id="3756" v="1" lv="1" ov="1" />
  <pa id="10141" v="2017-04-01" lv="2017-04-01" ov="2017-04-01" />
  <pa id="10142" v="6" lv="6" ov="6" />
  <pa id="10458" v="2" lv="2" ov="2" />
  <pa id="10461" v="1" lv="1" ov="1" />
  <pa id="10470" v="2" lv="2" ov="2" />
  <pa id="10471" v="4" lv="4" ov="4" />
  <pa id="10472" v="0" lv="0" ov="0" />
  <pa id="10545" v="0" lv="0" ov="0" />
  <pa id="10635" v="BENREKKAB BOUCHAIB" lv="BENREKKAB BOUCHAIB" ov="BENREKKAB BOUCHAIB" />
  <pa id="10636" v="FR7630003016500005123901162" lv="FR7630003016500005123901162" ov="FR7630003016500005123901162" />
  <pa id="10638" v="1943-01-01" lv="1943-01-01" ov="1943-01-01" />
  <pa id="10639" v="PONTOISE" lv="PONTOISE" ov="PONTOISE" />
  <pa id="10641" v="1" lv="1" ov="1" />
  <pa id="10643" v="6" lv="6" ov="6" />
  <pa id="10646" v="mbenrekkab@fr.scc.com" lv="mbenrekkab@fr.scc.com" ov="mbenrekkab@fr.scc.com" />
  <pa id="10647" v="BOUCHAIB" lv="BOUCHAIB" ov="BOUCHAIB" />
  <pa id="10650" v="1" lv="1" ov="1" />
  <pa id="10655" v="BENREKKAB" lv="BENREKKAB" ov="BENREKKAB" />
  <pa id="10657" v="2" lv="2" ov="2" />
  <pa id="10658" v="14" lv="14" ov="14" />
  <pa id="10659" v="1" lv="1" ov="1" />
  <pa id="10660" v="0130313851" lv="0130313851" ov="0130313851" />
  <pa id="10661" v="" lv="" ov="" />
  <pa id="10666" v="0" lv="0" ov="0" />
  <pa id="10668" v="" lv="" ov="" />
  <pa id="10670" v="" lv="" ov="" />
  <pa id="10672" v="HAUTS DE MARCOUVILLE" lv="HAUTS DE MARCOUVILLE" ov="HAUTS DE MARCOUVILLE" />
  <pa id="10673" v="" lv="" ov="" />
  <pa id="10675" v="10" lv="10" ov="10" />
  <pa id="10676" v="0" lv="0" ov="0" />
  <pa id="10677" v="95300" lv="95300" ov="95300" />
  <pa id="10721" v="2" lv="2" ov="2" />
  <pa id="10932" v="0" lv="0" ov="0" />
  <pa id="10933" v="101" lv="101" ov="101" />
  <pa id="10934" v="301" lv="301" ov="301" />
  <pa id="11044" v="1" lv="1" ov="1" />
  <pa id="11045" v="28097868" lv="28097868" ov="28097868" />
  <pa id="11086" v="0" lv="0" ov="0" />
  <pa id="11087" v="0" lv="0" ov="0" />
  <pa id="11088" v="0" lv="0" ov="0" />
  <pa id="11089" v="0" lv="0" ov="0" />
  <pa id="12108" v="6" lv="6" ov="6" />
  <pa id="12134" v="54" lv="54" ov="54" />
  <pa id="12181" v="++LE000000028097868" lv="++LE000000028097868" ov="++LE000000028097868" />
  <pa id="12185" v="2" lv="2" ov="2" />
  <pa id="12188" v="00:00" lv="00:00" ov="00:00" />
  <pa id="12213" v="" lv="" ov="" />
  <pa id="12231" v="1" lv="1" ov="1" />
  <pa id="12332" v="-1" lv="-1" ov="-1" />
  <pa id="12333" v="5" lv="5" ov="5" />
  <pa id="12356" v="00:00" lv="00:00" ov="00:00" />
  <pa id="12357" v="" lv="" ov="" />
  <pa id="12367" v="" lv="" ov="" />
  <pa id="12368" v="" lv="" ov="" />
  <pa id="12369" v="" lv="" ov="" />
  <pa id="12403" v="0" lv="0" ov="0" />
  <pa id="12484" v="1.034022" lv="1.034022" ov="1.034022" />
  <pa id="12485" v="1.034022" lv="1.034022" ov="1.034022" />
  <pa id="12486" v="400" lv="400" ov="400" />
  <pa id="12586" v="2011-03-02" lv="2011-03-02" ov="2011-03-02" />
  <pa id="12592" v="202" lv="202" ov="202" />
  <pa id="12601" v="4" lv="4" ov="4" />
  <pa id="12613" v="212.07" lv="212.07" ov="212.07" />
  <pa id="12614" v="2013-11-12" lv="2013-11-12" ov="2013-11-12" />
  <pa id="12622" v="39.54" lv="39.54" ov="39.54" />
  <pa id="12642" v="0" lv="0" ov="0" />
  <pa id="12700" v="243.18" lv="243.18" ov="243.18" />
  <pa id="12782" v="1" lv="1" ov="1" />
  <pa id="12783" v="" lv="" ov="" />
  <pa id="12786" v="PONTOISE" lv="PONTOISE" ov="PONTOISE" />
  <pa id="12787" v="95500" lv="95500" ov="95500" />
  <pa id="12788" v="6" lv="6" ov="6" />
  <pa id="12789" v="1" lv="1" ov="1" />
  <pa id="12790" v="3" lv="3" ov="3" />
  <pa id="12791" v="0" lv="0" ov="0" />
  <pa id="12792" v="2004-01-01" lv="2004-01-01" ov="2004-01-01" />
  <pa id="12793" v="2" lv="2" ov="2" />
  <pa id="12795" v="1" lv="1" ov="1" />
  <pa id="12796" v="" lv="" ov="" />
  <pa id="12797" v="hauts de marcouville" lv="hauts de marcouville" ov="hauts de marcouville" />
  <pa id="12798" v="10" lv="10" ov="10" />
  <pa id="12899" v="0" lv="0" ov="0" />
  <pa id="12900" v="3" lv="3" ov="3" />
  <pa id="12901" v="0" lv="0" ov="0" />
  <pa id="12904" v="FR7630003016500005123901162" lv="FR7630003016500005123901162" ov="FR7630003016500005123901162" />
  <pa id="12946" v="" lv="" ov="" />
  <pa id="12995" v="0" lv="0" ov="0" />
  <pa id="12996" v="" lv="" ov="" />
  <pa id="12997" v="" lv="" ov="" />
  <pa id="12998" v="" lv="" ov="" />
  <pa id="12999" v="" lv="" ov="" />
  <pa id="13019" v="3" lv="3" ov="3" />
  <pa id="13022" v="-1" lv="-1" ov="-1" />
  <pa id="13027" v="" lv="" ov="" />
  <pa id="13032" v="0" lv="0" ov="0" />
  <pa id="13037" v="1" lv="1" ov="1" />
  <pa id="13042" v="0" lv="0" ov="0" />
  <pa id="13043" v="0" lv="0" ov="0" />
  <pa id="13044" v="2" lv="2" ov="2" />
  <pa id="13045" v="" lv="" ov="" />
  <pa id="13046" v="0" lv="0" ov="0" />
  <pa id="13047" v="0" lv="0" ov="0" />
  <pa id="13048" v="1" lv="1" ov="1" />
  <pa id="13049" v="0" lv="0" ov="0" />
  <pa id="13050" v="60" lv="60" ov="60" />
  <pa id="13052" v="3" lv="3" ov="3" />
  <pa id="13053" v="0" lv="0" ov="0" />
  <pa id="13054" v="1" lv="1" ov="1" />
  <pa id="13055" v="" lv="" ov="" />
  <pa id="13056" v="" lv="" ov="" />
  <pa id="13057" v="0" lv="0" ov="0" />
  <pa id="13058" v="0" lv="0" ov="0" />
  <pa id="13059" v="" lv="" ov="" />
  <pa id="13060" v="" lv="" ov="" />
  <pa id="13109" v="95300" lv="95300" ov="95300" />
  <pa id="13130" v="1" lv="1" ov="1" />
  <pa id="13132" v="0" lv="0" ov="0" />
  <pa id="13172" v="1" lv="1" ov="1" />
  <pa id="13173" v="1" lv="1" ov="1" />
  <pa id="13318" v="14" lv="14" ov="14" />
  <pa id="13353" v="" lv="" ov="" />
  <pa id="13483" v="0" lv="0" ov="0" />
  <pa id="13485" v="-1" lv="-1" ov="-1" />
  <pa id="13559" v="" lv="" ov="" />
  <pa id="13560" v="" lv="" ov="" />
  <pa id="13656" v="0" lv="0" ov="0" />
  <pa id="13658" v="243.18" lv="243.18" ov="243.18" />
  <pa id="13787" v="0" lv="0" ov="0" />
  <pa id="13828" v="0" lv="0" ov="0" />
  <pa id="13840" v="0" lv="0" ov="0" />
  <pa id="13882" v="0" lv="0" ov="0" />
  <pa id="13888" v="0" lv="0" ov="0" />
  <pa id="13894" v="0" lv="0" ov="0" />
  <pa id="13900" v="0" lv="0" ov="0" />
  <pa id="13905" v="243.18" lv="243.18" ov="243.18" />
  <pa id="13906" v="212.07" lv="212.07" ov="212.07" />
  <pa id="13939" v="2" lv="2" ov="2" />
  <pa id="13940" v="-1" lv="-1" ov="-1" />
  <pa id="13941" v="1" lv="1" ov="1" />
  <pa id="13943" v="2" lv="2" ov="2" />
  <pa id="13957" v="" lv="" ov="" />
  <pa id="13958" v="" lv="" ov="" />
  <pa id="13959" v="" lv="" ov="" />
  <pa id="13960" v="" lv="" ov="" />
</Xml>')