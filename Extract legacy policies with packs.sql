USE [AGDF_Mario_Migration]
GO

WITH [CTE_Data] AS
(
	SELECT
		[part3].[CNT_ID] [PolicyNumber],
		CASE [part3].[CCV_PACK_ENV_IS_SELECTED] WHEN 1 THEN 1 ELSE 0 END [HasEnvironment],
		CASE [part3].[CCV_PACK_GARDEN_IS_SELECTED] WHEN 1 THEN 1 ELSE 0 END [HasGarden],
		CASE [part3].[CCV_PACK_TRAVEL_IS_SELECTED] WHEN 1 THEN 1 ELSE 0 END [HasTravel],
		CASE [part3].[CCV_PACK_SWIMMING_POOL_IS_SELECTED] WHEN 1 THEN 1 ELSE 0 END [HasSwimmingPool],
		CASE [part1].[OFR_PAS_ID]
			WHEN 1 THEN 4
			WHEN 3 THEN 1
			ELSE 0
		END [PaymentPeriod]
		FROM
			[FromLegacy].[LegacyPolicyPart3] [part3]
			JOIN [FromLegacy].[LegacyPolicyPart1] [part1]
				ON [part3].[CNT_ID] = [part1].[CNT_ID]
)
SELECT
	[data].[PolicyNumber] [Policy number],
	CASE [data].[HasEnvironment] WHEN 1 THEN 'Yes' ELSE 'No' END [Has environment],
	CASE [data].[HasGarden] WHEN 1 THEN 'Yes' ELSE 'No' END [Has garden],
	CASE [data].[HasTravel] WHEN 1 THEN 'Yes' ELSE 'No' END [Has travel],
	CASE [data].[HasSwimmingPool] WHEN 1 THEN 'Yes' ELSE 'No' END [Has swimming pool],
	CASE [data].[PaymentPeriod] WHEN 1 THEN 'Yearly' WHEN 4 THEN 'Monthly' ELSE '===' END [Payment period]
	FROM
		[CTE_Data] [data]
	WHERE
		[data].[PaymentPeriod] = 4
		AND ([data].[HasEnvironment] + [data].[HasGarden] + [data].[HasTravel] + [data].[HasSwimmingPool] > 1)