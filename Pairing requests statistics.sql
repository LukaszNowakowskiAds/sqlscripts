USE [GBI_Logging]
GO

DECLARE @StartDate DATETIME2 = '2017-07-04 00:00:00'
DECLARE @EndDate DATETIME2 = '2017-07-05 00:00:00'

DECLARE @Requests TABLE
(
	[HttpMethod] NVARCHAR(20) NOT NULL,
	[Url] NVARCHAR(MAX) NOT NULL,
	[ResponseCodeId] SMALLINT NOT NULL,
	[ResponseMessage] NVARCHAR(MAX)
)

INSERT INTO @Requests
	SELECT
		[messages].[HttpMethod],
		[messages].[Url],
		[messages].[ResponseCodeId],
		[contents].[ResponseMessage]
		FROM
			[dbo].[HttpMessageAudit] [messages]
			JOIN [dbo].[HttpMessageAuditContent] [contents]
				ON [messages].[Id] = [contents].[Id]
		WHERE
			[messages].[RequestTime] BETWEEN @StartDate AND @EndDate
			AND [messages].[Url] LIKE '%/device%'

DECLARE @Succeeded INT
SELECT
	@Succeeded = COUNT(1)
	FROM
		@Requests [requests]
	WHERE
		[requests].[ResponseCodeId] = 200

DECLARE @SimNotFound INT
SELECT
	@SimNotFound = COUNT(1)
	FROM
		@Requests [requests]
	WHERE
		[requests].[ResponseCodeId] = 404
		AND [requests].[ResponseMessage] LIKE '{"status":"SIM Not Found"}%'

DECLARE @DeviceNotFound INT
SELECT
	@DeviceNotFound = COUNT(1)
	FROM
		@Requests [requests]
	WHERE
		[requests].[ResponseCodeId] = 404
		AND [requests].[ResponseMessage] LIKE '{"status":"Device Not Found"}%'

DECLARE @OtherErrors INT
SELECT
	@OtherErrors = COUNT(1) - @Succeeded - @SimNotFound - @DeviceNotFound
	FROM
		@Requests

SELECT
	@Succeeded [Succeeded],
	@SimNotFound [Sim not found],
	@DeviceNotFound [Device not found],
	@OtherErrors [Other errors]
