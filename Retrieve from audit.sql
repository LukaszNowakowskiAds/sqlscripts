WITH [CTE_Events] AS
(
	SELECT
		data = CONVERT(XML, event_data)
	FROM
		sys.fn_xe_file_target_read_file('C:\Audits\EXEC_CoveragesRetrieval*.xel', 'C:\Audits\EXEC_CoveragesRetrieval*.xem', NULL, NULL)
),
tab AS
(
	SELECT
		[host] = data.value('(event/action[@name="client_hostname"]/value)[1]','nvarchar(400)'),
		app_name = data.value('(event/action[@name="client_app_name"]/value)[1]','nvarchar(400)'),
		username = data.value('(event/action[@name="username"]/value)[1]','nvarchar(400)'),
		[object_name] = data.value('(event/data[@name="object_name"]/value)[1]','nvarchar(250)'),
		[timestamp] = data.value('(event/@timestamp)[1]','datetime2')
	FROM
		[CTE_Events]
)
SELECT
	[host],
	app_name,
	username,
	[timestamp],
	[object_name]
FROM
	tab 
WHERE
	[object_name] IN ('FetchLimitsByPolicyVersion', 'FetchDeductiblesByPolicyVersion', 'FetchCoveragesByPolicyVersion')
	AND [timestamp] > '2023-08-24 12:13:00'
ORDER BY
	[object_name] ASC,
	[timestamp] DESC