USE [master]
GO

DECLARE @DatabaseName NVARCHAR(50) = 'AGDF_CommercialOffers'
DECLARE @DataDirectory NVARCHAR(300)
DECLARE @LogsDirectory NVARCHAR(300)

SELECT
    @DataDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultDataPath')),
    @LogsDirectory = CONVERT(NVARCHAR(MAX), serverproperty('InstanceDefaultLogPath'))

IF EXISTS (SELECT null FROM master.dbo.sysdatabases WHERE name = 'AGDF_CommercialOffers')
BEGIN
	ALTER DATABASE [AGDF_CommercialOffers] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [AGDF_CommercialOffers]
END

DECLARE @DataFile NVARCHAR(300) = @DataDirectory + @DatabaseName + '.mdf'
DECLARE @LogsFile NVARCHAR(300) = @LogsDirectory + @DatabaseName + '_log.ldf'

DECLARE @BackupPath NVARCHAR(200) = 'C:\Backups\AGDF_CommercialOffers.bak'
RESTORE DATABASE [AGDF_CommercialOffers]
	FROM DISK = @BackupPath
	WITH
		MOVE N'AGDF_CommercialOffers' TO @DataFile,
		MOVE N'AGDF_CommercialOffers_log' TO @LogsFile;
GO

USE [AGDF_CommercialOffers]
GO
CREATE USER [IIS APPPOOL\axa] FOR LOGIN [IIS APPPOOL\axa]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\axa]
GO

USE [AGDF_CommercialOffers]
GO
DBCC SHRINKFILE (N'AGDF_CommercialOffers_log' , 0, TRUNCATEONLY)
DBCC SHRINKFILE (N'AGDF_CommercialOffers' , 0, TRUNCATEONLY)
DBCC SHRINKDATABASE(N'AGDF_CommercialOffers' )
GO
